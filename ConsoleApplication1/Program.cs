﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Silanis.ESL.SDK;
using Silanis.ESL.SDK.Builder;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Environment.CurrentDirectory + @"\documents\TestPDF.pdf";
            FileStream testPDF = new FileStream(path, FileMode.Open, FileAccess.Read);

            EslClient eslClient = new EslClient(Properties.Resources.eslApiKey, Properties.Resources.eslUrl+"/api");
            DocumentPackage superDuperPackage = PackageBuilder.NewPackageNamed("GettingStartedExample: " + DateTime.Now)
                .WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings())
                    .WithSigner(SignerBuilder.NewSignerWithEmail(Properties.Resources.signer1Email)
                                .WithFirstName("John1")
                                .WithLastName("Smith1")
                                .WithCustomId("SIGNER1"))
                    .WithDocument(DocumentBuilder.NewDocumentNamed("Test Document")
                                  .WithId("testdocument1")
                                  .FromStream(testPDF, DocumentType.PDF)
                                  .WithSignature(SignatureBuilder.SignatureFor(Properties.Resources.signer1Email)
                                   .OnPage(0)
                                   .AtPosition(100, 100))
                                  .WithSignature(SignatureBuilder.InitialsFor(Properties.Resources.signer1Email)
                                   .OnPage(0)
                                   .AtPosition(100, 200))
                                  .WithSignature(SignatureBuilder.CaptureFor(Properties.Resources.signer1Email)
                                   .OnPage(0)
                                   .AtPosition(100, 300))
                                  )
                    .Build();

            PackageId packageId = eslClient.CreatePackage(superDuperPackage);
            eslClient.SendPackage(packageId);

            Console.WriteLine(Properties.Resources.eslUrl+"/api/packages/"+packageId.Id);

            // Optionally, get the session token for integrated signing.
            SessionToken sessionToken = eslClient.SessionService.CreateSessionToken(packageId, "SIGNER1");

            Console.WriteLine(Properties.Resources.eslUrl + "/access?sessionToken=" + sessionToken.ToString());

            SigningStatus signingStatus = eslClient.PackageService.GetSigningStatus(packageId, "SIGNER1", "testdocument1");
            Console.WriteLine("signing status: " + signingStatus.ToString());

            DocumentPackage documentPackage = eslClient.GetPackage(packageId);
            Console.WriteLine("document package id: " + documentPackage.Id);

            Console.WriteLine("Press ESC to stop");
            do
            {
                while (!Console.KeyAvailable)
                {
                    // Do something
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
