# README #

## Configuring the Getting Started Application ##

Make sure you customize the following project `properties` in file `/Properties/Resources.resx`:

```
eslApiKey = YOUR ESL API KEY
eslURL = https://sandbox.e-signlive.com
signer1Email = esl.john.smith@mailinator.com
```